#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

typedef enum{
    PERM_DEN = 2,
    NO_FILE_DIR = 3,
    ERR = 4
} Error;

enum {LENGTH = 10000 };

void print_str(int fd, char *str){
    write(fd, str, strlen(str));
}

Error print_err(char *name){
    print_str(2, "mv: ");
    if (name != NULL){
        print_str(2, name);
        print_str(2, ": ");
    }

    if (errno == EACCES){
        print_str(2, "Permission denied\n");
        return PERM_DEN;
    } else {
        if (errno == ENOENT) {
            print_str(2, "No such file\n");
            return NO_FILE_DIR;
        } else {
            print_str(2, "Error\n");
            return ERR;
    }
}

int is_equal(char *first, char *second){
    struct stat st1, st2;
    stat(first, &st1);
    stat(second, &st2);
    return (st1.st_ino == st2.st_ino);
}

int is_reference(char *name){
    struct stat st, lst;
    stat(name, &st);
    stat (name, &lst);
    return (st.st_ino == lst.st_ino);
}

int main(int argc, char **argv){
    if (argc != 3){
        print_str(2, "mv: Wrong arguments number\n");
        return 1;
    }

    int fd_src = open(argv[1], O_RDONLY);
    if (fd_src == -1){
        print_err(argv[1]);
        return 1;
    }
    unlink(argv[2]);
    int fd_dest = open(argv[2], O_CREAT | O_WRONLY, 0777);
    if (fd_dest == -1) {
        print_err(argv[2]);
        return 1;
    }

    if (is_equal(argv[1], argv[2])){
        close(fd_src);
        close(fd_dest);
        return 1;
    }
    char buffer[LENGTH];
    long len;
    while (0 < (len = read(fd_src, buffer, LENGTH))) {
        write(fd_dest, buffer, len);
    }
    unlink(argv[1]);
    return 0;
}
