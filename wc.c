#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>

enum {LENGTH = 2 };

struct Stat {
    long line;
    long word;
    long sym;
};

void print_str(int fd ,char *str)
{
    write(fd, str, strlen(str));
}

void print_num(int fd, long num)
{
    char buffer[100];
    int len = 0;

    if (num <= 0) {
        buffer[0] = '0';
        len = 1;
    } else {
        while (num > 0) {
            buffer[len++] = (char) ('0' + (num % 10));
            num /= 10;
        }
        for (int i = 0; i < len / 2; i++) {
            char swap = buffer[i];
            buffer[i] = buffer[len - 1 - i];
            buffer[len - 1 - i] = swap;
        }
    }

    write(fd, buffer, len);
}

void print_stat(int fd, struct Stat stat)
{
    print_num(fd, stat.line);
    print_str(fd, " ");
    print_num(fd, stat.word);
    print_str(fd, " ");
    print_num(fd, stat.sym);
}

int main(int argc, char **argv)
{
    struct Stat total = {0, 0,0};

    if (argc == 1) {
        goto JMP;
    }

    for (int i = 1; i < argc; i++)
    {
        int fd = open(argv[i], O_RDONLY);
        if (fd == -1) {
            print_str(1, "wc: ");
            print_str(1, argv[i]);
            print_str(1, ": No such file\n");
            continue;
        }

        JMP:
        if (argc == 1) {
            i = argc;
            fd = 0;
            argv[i] = "total";
        }

        struct Stat current_stat = {0, 0, 0};
        char buffer[LENGTH];
        long length;
        int word_now = 0;
        int line_now = 0;

        while (1)
        {
            length = read(fd, buffer, sizeof buffer);
            if (length == 0) break;

            current_stat.sym += length;

            for (long j = 0; j < length; j++) {
                if (!line_now) {
                    current_stat.line++;
                    line_now = 1;
                }

                if (buffer[j] == '\n') {
                    line_now = 0;
                }

                if (isspace(buffer[j])) {
                    if (word_now) {
                        current_stat.word++;
                    }
                    word_now = 0;
                } else {
                    word_now = 1;
                }
            }
        }

        current_stat.word += word_now;

        print_str(1, argv[i]);
        print_str(1, " ");
        print_stat(1, current_stat);
        print_str(1, "\n");

        total.line += current_stat.line;
        total.word += current_stat.word;
        total.sym += current_stat.sym;
    }

    if (argc > 2) {
        print_str(1, "total ");
        print_stat(1, total);
        print_str(1, "\n");
    }

    return 0;
}
