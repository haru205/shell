#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

enum { LENGTH = 10000 };

typedef enum {
    LAST,
    BEGIN
} Mode;
typedef enum {
    OK,
    WR_NUM_TOK,
    NO_SUCH_MODE,
    TOO_MANY_ARG,
    PERM_DEN,
    NO_SUCH_FILE,
    MODE_REPEAT,
    FILE_REPEAT
} Error;

static long n = 10;
static Mode mode = LAST;
static int mode_cnt = 0;
static Error err = OK;
static int fd_src = 0;

void print_str(char *str)
{
    write(1, str, strlen(str));
}

void print_err(char *arg)
{
    print_str("tail: ");

    if (arg != NULL) {
        print_str(arg);
        print_str(": ");
    }

    if (err == TOO_MANY_ARG) {
        print_str("Too many arguments\n");
    } else if (err == WR_NUM_TOK) {
        print_str("Wrong value token\n");
    } else if (err == NO_SUCH_MODE) {
        print_str("No such mode\n");
    } else if (err == NO_SUCH_FILE) {
        print_str("No such file\n");
    } else if (err == PERM_DEN) {
        print_str("Permission denied\n");
    } else {
        print_str("Error\n");
    }
}

void process_arg(const char *arg)
{
    long val;
    char *end;

    if (arg[0] == '+') {
        if (mode_cnt > 0) {
            err = MODE_REPEAT;
            return;
        }

        val = strtol(arg + 1, &end, 10);
        if (errno != 0 || end - arg != strlen(arg) || end - arg <= 1) {
            err = WR_NUM_TOK;
            return;
        }

        mode_cnt++;
        mode = BEGIN;
        n = val;
        return;
    }

    if (arg[0] == '-') {
        if (mode_cnt > 0) {
            err = MODE_REPEAT;
            return;
        }

        val = strtol(arg + 1, &end, 10);
        if (errno != 0 || end - arg != strlen(arg) || end - arg <= 1) {
            err = WR_NUM_TOK;
            return;
        }

        mode_cnt++;
        mode = LAST;
        n = val;
        return;
    }

    err = NO_SUCH_MODE;
}

void process_file(char *file_name)
{
    if (fd_src != 0) {
        err = FILE_REPEAT;
        return;
    }

    int fd = open(file_name, O_RDONLY);
    if (fd == -1) {
        if (errno == EACCES) {
            err = PERM_DEN;
        } else {
            err = NO_SUCH_FILE;
        }
        return;
    }
    fd_src = fd;
}

int safe_file()
{
    char name[] = "tempXXXXXX";
    int fd = mkstemp(name);
    unlink(name);
    return fd;
}

int main(int argc, char **argv)
{
    if (argc > 3) {
        err = TOO_MANY_ARG;
        print_err(NULL);
        return 1;
    }

    if (argc > 1) {
        for (int i = 1; i < argc; i++) {
            process_arg(argv[i]);
            if (err != OK) {
                if (err != NO_SUCH_MODE) {
                    print_err(argv[i]);
                    if (fd_src != 0) {
                        close(fd_src);
                    }
                    return 1;
                }

                err = OK;
                process_file(argv[i]);
                if (err != OK) {
                    print_err(argv[i]);
                    return 1;
                }
            }
        }
    }

    char buffer[LENGTH];
    long length;
    long line_cnt = 0;
    int line_now = 0;

    int is_file = 1;
    if (-1 == lseek(fd_src, 1, SEEK_SET)) {
        is_file = 0;
    } else {
        lseek(fd_src, 0, SEEK_SET);
    }

    if (mode == LAST && !is_file)
    {
        int *fd_mas = malloc(sizeof (int) * n);
        int mas_size = 0;

        while (1) {
            length = read(fd_src, buffer, sizeof buffer);
            if (length == 0) break;

            for (long j = 0; j < length; j++) {
                if (!line_now) {
                    line_cnt++;
                    line_now = 1;

                    if (mas_size < n) {
                        fd_mas[mas_size++] = safe_file();
                    } else {
                        close(fd_mas[0]);
                        for (int i = 0; i < mas_size - 1; i++) {
                            fd_mas[i] = fd_mas[i + 1];
                        }
                        fd_mas[mas_size - 1] = safe_file();
                    }
                }

                write(fd_mas[mas_size - 1], buffer + j, 1);

                if (buffer[j] == '\n') {
                    line_now = 0;
                }
            }
        }

        for (int i = 0; i < mas_size; i++) {
            lseek(fd_mas[i], 0, SEEK_SET);
            while (0 < (length = read(fd_mas[i], buffer, LENGTH))) {
                write(1, buffer, length);
            }
        }
    }
    else if (mode == LAST && is_file)
    {
        long *pos = malloc(sizeof (long) * n);
        int size = 0;
        long cur_pos = 0;

        while (1) {
            length = read(fd_src, buffer, sizeof buffer);
            if (length == 0) break;

            for (long j = 0; j < length; j++) {
                if (!line_now) {
                    line_cnt++;
                    line_now = 1;

                    if (size < n) {
                        pos[size++] = cur_pos;
                    } else {
                        for (int i = 0; i < size - 1; i++) {
                            pos[i] = pos[i + 1];
                        }
                        pos[size - 1] = cur_pos;
                    }
                }
                if (buffer[j] == '\n') {
                    line_now = 0;
                }
                cur_pos++;
            }
        }

        lseek(fd_src, pos[0], SEEK_SET);
        while (0 < (length = read(fd_src, buffer, LENGTH))) {
            write(1, buffer, length);
        }
    }
    else if (mode == BEGIN)
    {
        while (1) {

            length = read(fd_src, buffer, sizeof buffer);
            if (length == 0) break;

            long j;
            for (j = 0; j < length; j++) {
                if (!line_now) {
                    line_cnt++;
                    if (line_cnt > n) {
                        write(1, buffer + j, length - j);
                        break;
                    }
                    line_now = 1;
                }

                if (buffer[j] == '\n') {
                    line_now = 0;
                }
            }

            while (0 < (length = read(fd_src, buffer, LENGTH))) {
                write(1, buffer, length);
            }
        }
    }

    if (is_file) {
        close(fd_src);
    }

    return 0;
}
